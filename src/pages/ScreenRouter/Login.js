import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {
  StatusBar,
  ScrollView,
  View,
  KeyboardAvoidingView,
  Text,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import {SafeAreaView, useSafeAreaInsets} from 'react-native-safe-area-context';
import * as LocalAuthentication from 'expo-local-authentication';
import {useMutation} from 'react-query';
import {useForm} from 'react-hook-form';
import {showMessage} from 'react-native-flash-message';
import {yupResolver} from '@hookform/resolvers/yup';
import auth from '@react-native-firebase/auth';
import analytics from '@react-native-firebase/analytics';
import {GoogleSignin} from '@react-native-google-signin/google-signin';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {loginValidation} from '../../configs/form-validation';
import {Input} from '../../components';
import styles from '../../styles/pages/Login';

function Login({navigation}) {
  const [authenticationType, setAuthenticationType] = useState('dialpad');
  const insets = useSafeAreaInsets();

  GoogleSignin.configure({
    webClientId:
      '168380373530-6lthabg11v99vglt9k0igcrp47ug5gf1.apps.googleusercontent.com',
  });

  const {
    control,
    handleSubmit,
    formState: {errors},
  } = useForm({
    defaultValues: {
      email: '',
      password: '',
    },
    resolver: yupResolver(loginValidation),
  });

  const {mutate: loginMutation, isLoading} = useMutation(
    async loginData => {
      let response;
      if (loginData.type === 'email-password') {
        response = await auth().signInWithEmailAndPassword(
          loginData.data.email,
          loginData.data.password,
        );
      } else if (loginData.type === 'google-oauth') {
        response = await auth().signInWithCredential(loginData.data);
      } else {
        console.error('[ERROR] Missing login type.');
      }
      return response;
    },
    {
      throwOnError: true,
      onSuccess: data => {
        navigation.replace('Main');
      },
      onError: error => {
        showMessage({
          message: error.message,
          type: 'danger',
          backgroundColor: EStyleSheet.value('$danger[500]'),
          titleStyle: {textAlign: 'center'},
          floating: true,
          statusBarHeight: insets.top + 6,
        });
      },
    },
  );

  const onSubmit = async data => {
    loginMutation({
      type: 'email-password',
      data: data,
    });
  };

  const onGoogleLogin = async () => {
    const {idToken} = await GoogleSignin.signIn();
    const googleCredential = auth.GoogleAuthProvider.credential(idToken);
    loginMutation({
      type: 'google-oauth',
      data: googleCredential,
    });
  };

  const useLocalAuth = async () => {
    await LocalAuthentication.authenticateAsync().then(status => {
      status.success && navigation.replace('Main');
    });
  };

  useEffect(() => {
    LocalAuthentication.supportedAuthenticationTypesAsync().then(type => {
      let icon = 'dialpad';
      if (type[0] === 1) {
        icon = 'fingerprint';
      } else if (type[0] === 2) {
        icon = 'face-recognition';
      } else if (type[0] === 3) {
        icon = 'eye';
      }
      setAuthenticationType(icon);
    });
    if (auth().currentUser) {
      // eslint-disable-next-line react-hooks/rules-of-hooks
      useLocalAuth();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <StatusBar
        translucent
        backgroundColor="transparent"
        barStyle="dark-content"
      />
      <SafeAreaView style={styles.safeArea}>
        <ScrollView contentContainerStyle={styles.container}>
          <KeyboardAvoidingView behavior="position" style={styles.form}>
            <Text style={styles.formHeader}>Login</Text>
            <Text style={styles.formDescription}>
              Access your account with Login.
            </Text>
            <Input
              name="email"
              placeholder="Email"
              icon="at-sign"
              testID="email_input"
              keyboardType="email-address"
              control={control}
              errors={errors}
              disable={isLoading}
            />
            <Input
              name="password"
              placeholder="Password"
              icon="lock"
              testID="password_input"
              control={control}
              errors={errors}
              disable={isLoading}
              secureTextEntry
            />
            <TouchableOpacity
              testID="forgot_link"
              onPress={() => navigation.navigate('Forgot Password')}>
              <Text style={styles.forgotLink}>Forgot Password?</Text>
            </TouchableOpacity>
            <View style={styles.formButtonGroup}>
              <View style={styles.rowButtonGroup}>
                <TouchableOpacity
                  testID="login_button"
                  style={[
                    styles.formButton,
                    !isLoading
                      ? styles.formButtonActive
                      : styles.formButtonDisabled,
                  ]}
                  onPress={handleSubmit(onSubmit)}
                  disabled={isLoading}>
                  {isLoading && (
                    <ActivityIndicator
                      size="small"
                      style={styles.formButton.indicator}
                      color={styles.formButton.indicator.color}
                    />
                  )}
                  <Text style={styles.formButtonText}>Login</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  testID="biometric_button"
                  style={[
                    styles.biometricButton,
                    auth().currentUser
                      ? styles.formButtonActive
                      : styles.formButtonDisabled,
                  ]}
                  onPress={useLocalAuth}
                  disabled={!auth().currentUser}>
                  <Icon name={authenticationType} size={20} color={'white'} />
                </TouchableOpacity>
              </View>
              <TouchableOpacity
                testID="googleoauth_button"
                style={[
                  styles.googleSigninButton,
                  !isLoading
                    ? styles.formButtonActive
                    : styles.formButtonDisabled,
                ]}
                onPress={onGoogleLogin}
                disabled={isLoading}>
                <Icon name="google" style={styles.googleSigninButton.icon} />
                <Text style={styles.formButtonText}>Sign in with Google</Text>
              </TouchableOpacity>
            </View>

            <View style={styles.navigationContainer}>
              <Text style={styles.navigationText}>
                Don&apos;t have an account?
              </Text>
              <TouchableOpacity
                testID="register_link"
                onPress={() => navigation.navigate('Register')}>
                <Text style={styles.navigationLink}>Register</Text>
              </TouchableOpacity>
            </View>
          </KeyboardAvoidingView>
        </ScrollView>
      </SafeAreaView>
    </>
  );
}

Login.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
    replace: PropTypes.func.isRequired,
  }).isRequired,
};

export default Login;
