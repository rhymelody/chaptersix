import React, {useState} from 'react';
import {Alert} from 'react-native';
import {CameraScreen, CameraType} from 'react-native-camera-kit';
import {useIsFocused} from '@react-navigation/native';

const QRScanner = () => {
  const [cooldown, setCooldown] = useState(false);
  const isFocused = useIsFocused();

  const onReadCode = event => {
    if (!cooldown) {
      Alert.alert('QR Scanner', event.nativeEvent.codeStringValue);
      setCooldown(value => !value);
    }
    setTimeout(() => setCooldown(value => !value), 1500);
  };

  return isFocused ? (
    <CameraScreen
      cameraType={CameraType.Back}
      onReadCode={event => onReadCode(event)}
      focusMode="off"
      zoomMode="off"
      laserColor="red"
      frameColor="white"
      showFrame
      scanBarcode
    />
  ) : null;
};

export default QRScanner;
