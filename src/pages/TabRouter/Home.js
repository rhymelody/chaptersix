import React from 'react';
import {StatusBar, Text, TouchableOpacity} from 'react-native';
import {SafeAreaView, useSafeAreaInsets} from 'react-native-safe-area-context';
import EStyleSheet from 'react-native-extended-stylesheet';
import {useMutation} from 'react-query';
import {showMessage} from 'react-native-flash-message';
import auth from '@react-native-firebase/auth';
import Icon from 'react-native-vector-icons/FontAwesome';

import {Maps} from '../../components';
import styles from '../../styles/pages/Home';

const Home = ({navigation}) => {
  const today = new Date();
  const currentHour = today.getHours();
  const insets = useSafeAreaInsets();
  const user = auth().currentUser;

  const getGreeting = () => {
    if (currentHour < 12) {
      return 'Good Morning';
    }
    if (currentHour < 18) {
      return 'Good Afternoon';
    }
    return 'Good Evening';
  };

  const {mutate: logOutMutation} = useMutation(
    async () => {
      const response = await auth().signOut();
      return response;
    },
    {
      throwOnError: true,
      onSuccess: () => {
        showMessage({
          message: 'Log out success.',
          type: 'danger',
          backgroundColor: EStyleSheet.value('$success[500]'),
          titleStyle: {textAlign: 'center'},
          floating: true,
          statusBarHeight: insets.top + 6,
        });
        navigation.replace('Login');
      },
      onError: error => {
        showMessage({
          message: error.message,
          type: 'danger',
          backgroundColor: EStyleSheet.value('$danger[500]'),
          titleStyle: {textAlign: 'center'},
          floating: true,
          statusBarHeight: insets.top + 6,
        });
      },
    },
  );

  return (
    <>
      <StatusBar
        translucent
        backgroundColor="transparent"
        barStyle="light-content"
      />
      <SafeAreaView style={styles.header} edges={['right', 'top', 'left']}>
        <Text style={styles.headerGreeting} testID="greeting_text">
          {getGreeting()}, {'\n'}
          <Text style={styles.headerName}>
            {user ? (user.displayName ? user.displayName : user.email) : 'User'}
          </Text>
        </Text>
        <TouchableOpacity onPress={logOutMutation}>
          <Icon name="power-off" style={styles.headerLogOut} />
        </TouchableOpacity>
      </SafeAreaView>
      <SafeAreaView style={styles.safeArea} edges={['right', 'bottom', 'left']}>
        <Maps />
      </SafeAreaView>
    </>
  );
};

export default Home;
