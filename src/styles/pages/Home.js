import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  safeArea: {
    backgroundColor: '$background',
    flexGrow: 1,
    paddingBottom: '6.375rem',
  },
  header: {
    flexGrow: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '$primary[500]',
    padding: '1.5rem',
  },
  headerGreeting: {
    color: '$base.white',
    fontSize: '1rem',
    fontWeight: '400',
  },
  headerName: {
    color: '$base.white',
    fontSize: '1rem',
    fontWeight: '500',
  },
  headerLogOut: {
    fontSize: '1.5rem',
    color: '$base.white',
  },
  container: {
    backgroundColor: '$background',
    padding: '1.5rem',
    width: '100%',
    flexGrow: 1,
  },
});

export default styles;
