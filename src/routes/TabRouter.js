import React from 'react';
import EStyleSheet from 'react-native-extended-stylesheet';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/FontAwesome';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

import {Home, QRScanner} from '../pages/TabRouter';

const TabRouter = () => {
  const Tab = createBottomTabNavigator();
  const insets = useSafeAreaInsets();

  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarActiveTintColor: EStyleSheet.value("$primary['500']"),
        tabBarInactiveTintColor: EStyleSheet.value("$secondary['500']"),
        tabBarStyle: {
          height: insets.bottom + 58,
          paddingTop: 8,
          paddingBottom: insets.bottom || 8,
        },
      }}>
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarIcon: ({color, size}) => (
            <Icon name="home" size={size} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name="QR Scanner"
        component={QRScanner}
        options={{
          tabBarIcon: ({color, size}) => (
            <Icon name="qrcode" size={size} color={color} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default TabRouter;
