import React, {useEffect, useState} from 'react';
import {Platform} from 'react-native';
import {check, request, PERMISSIONS, RESULTS} from 'react-native-permissions';

import EStyleSheet from 'react-native-extended-stylesheet';
import MapView, {
  PROVIDER_GOOGLE,
  Marker,
  enableLatestRenderer,
} from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service';

const styles = EStyleSheet.create({
  map: {
    width: '100%',
    height: '100%',
  },
});

EStyleSheet.build();

const Maps = () => {
  const [location, setLocation] = useState(null);

  const handleLocationPermission = async () => {
    let permissionCheck = '';
    if (Platform.OS === 'ios') {
      permissionCheck = await check(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE);

      if (permissionCheck === RESULTS.DENIED) {
        const permissionRequest = await request(
          PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
        );
        permissionRequest === RESULTS.GRANTED
          ? console.warn('Location permission granted.')
          : console.warn('Location perrmission denied.');
      }
    }

    if (Platform.OS === 'android') {
      permissionCheck = await check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION);

      if (permissionCheck === RESULTS.DENIED) {
        const permissionRequest = await request(
          PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
        );
        permissionRequest === RESULTS.GRANTED
          ? console.warn('Location permission granted.')
          : console.warn('Location perrmission denied.');
      }
    }
  };

  useEffect(() => {
    handleLocationPermission();

    Geolocation.getCurrentPosition(
      position => {
        const {latitude, longitude} = position.coords;
        setLocation({latitude, longitude});
      },
      error => {
        console.log(error.code, error.message);
      },
      {
        enableHighAccuracy: true,
        forceRequestLocation: true,
        forceLocationManager: true,
        showLocationDialog: true,
      },
    );
    enableLatestRenderer();
  }, []);

  return (
    <MapView
      provider={PROVIDER_GOOGLE}
      style={styles.map}
      initialRegion={
        location && {
          latitude: location.latitude,
          longitude: location.longitude,
          latitudeDelta: 0.015,
          longitudeDelta: 0.0121,
        }
      }
      loadingEnabled
      showsCompass
      showsBuildings
      showsUserLocation
      showsMyLocationButton
      followsUserLocation>
      <Marker
        coordinate={{
          latitude: -7.596950531005859,
          longitude: 112.79108315008534,
        }}
        title="My House"
        description="This is my house location."
      />
      <Marker
        coordinate={{
          latitude: -7.6003034,
          longitude: 112.7920155,
        }}
        title="SMK Negeri 1 Bangil"
        description="This is my vocational high school."
      />
      <Marker
        coordinate={{
          latitude: -7.5833967,
          longitude: 112.7928972,
        }}
        title="SMP Negeri 2 Bangil"
        description="This is my junior school."
      />
      <Marker
        coordinate={{
          latitude: -7.602215,
          longitude: 112.78544,
        }}
        title="Grandma's House"
        description="This is my grandma's house."
      />
      <Marker
        coordinate={{
          latitude: -7.5978314,
          longitude: 112.7846005,
        }}
        title="Alun-Alun Bangil"
        description="This is Alun-Alun Bangil."
      />
      <Marker
        coordinate={{
          latitude: -7.3143727,
          longitude: 112.7809842,
        }}
        title="Superindo MERR"
        description="This is Superindo MERR."
      />
      <Marker
        coordinate={{
          latitude: -7.3150372,
          longitude: 112.7810553,
        }}
        title="Subway MERR"
        description="This is Subway MERR."
      />
      <Marker
        coordinate={{
          latitude: -7.2745942,
          longitude: 112.7819782,
        }}
        title="Galaxy Mall"
        description="This is Galaxy Mall."
      />
      <Marker
        coordinate={{
          latitude: -7.2758471,
          longitude: 112.7937557,
        }}
        title="Politeknik Elektronika Negeri Surabaya"
        description="This is My Campus."
      />
    </MapView>
  );
};

export default Maps;
